from datetime import timedelta

from service_registry import db


def register_replica(replica_name, replica_host, service_port, ttl_web_app:int=180, ttl_replica:int=90):
    """Register a service with the registry"""

    # register replica
    db.redis_connection.lpush('web_app', replica_name)

    if ttl_web_app:
        db.redis_connection.expire('web_app', timedelta(seconds=ttl_web_app)) # require a heartbeat every 90 seconds


    db.redis_connection.hset(replica_name, "host", replica_host)
    db.redis_connection.hset(replica_name, "port", service_port)

    if ttl_replica:
        db.redis_connection.expire(replica_name, timedelta(seconds=ttl_replica)) # require a heartbeat every 90 seconds
