import os

from service_registry import db


def _get_secret_number() -> int:
    if "SECRET_NUMBER" in os.environ:
        return os.getenv("SECRET_NUMBER")
    else:
        raise Exception("SECRET_NUMBER is not set in env")


def init_secret_number():
    if db.secret_number is None:
        db.secret_number = _get_secret_number()
    else:
        pass
