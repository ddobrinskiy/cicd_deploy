FROM continuumio/miniconda3
RUN conda install flask requests -y --quiet

COPY . /lib
WORKDIR /lib

ENV PYTHONPATH=$PYTHONPATH:/lib

CMD ["python", "main.py"]