import time

from flask import Flask, jsonify

from service_registry import db, init_secret_number

app = Flask(__name__)


@app.route("/")
def hello_world():
    return jsonify(msg="Hello, World!")


@app.route("/return_secret_number")
def return_secret_number():
    time.sleep(1)
    return jsonify(secret_number=db.secret_number)


print("getting secret number...")
init_secret_number()
print(f"Sucess! secret_number: {db.secret_number}")

if __name__ == "__main__":
    print("getting secret number...")
    init_secret_number()
    print(f"Sucess! secret_number: {db.secret_number}")

    app.run(host="0.0.0.0")
